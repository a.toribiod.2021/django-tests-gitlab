from django.test import TestCase
from .views import PageCounter
from django.test import Client

# Create your tests here.


class CounterTest(TestCase):
    def test_counter(self):
        """
        Test that the counter starts at 0
        """
        counter = PageCounter()
        self.assertEqual(counter.get_count(), 0)

    def test_increment(self):
        """
        Test that the counter increments when we call the increment method
        """
        counter = PageCounter()
        counter.increment()
        self.assertEqual(counter.get_count(), 1)


class PageTest(TestCase):
    def test_ok(self):
        """
        Tests 200 OK
        """
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)
